# Create a new folder named "fastqc_practice" in your home directory
mkdir ~/fastqc_practice

# Execute fastqc program to process an example FASTQ file and save the output in the folder you just created
/home/shared/FastQC/fastqc \
	/home/shared/micr8130/example.fastq \
	-o ~/fastqc_practice
